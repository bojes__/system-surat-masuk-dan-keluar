<!DOCTYPE html>
<html>
<head>
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootplus/css/bootplus.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootplus/css/bootplus.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootplus/css/bootplus-responsive.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('fa/css/font-awesome.css'); ?>">
  <!-- <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.css');?>" rel="stylesheet"> -->
  <script src="<?php echo base_url('assets/js/tinymce/tinymce.min.js');?>"></script>

<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->
  <script src="<?php echo base_url('jquery/jquery.min.js'); ?>"></script> 
  <script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js'); ?>"></script>
  
  <script type="text/javascript">
    $(function () { $("[data-toggle='tooltip']").tooltip(); });</script>

    <script type="text/javascript">$(document).ready(function() {
      $('a[data-confirm]').click(function(ev) {
        var href = $(this).attr('href');
        if (!$('#dataConfirmModal').length) {
          $('body').append('<div id="dataConfirmModal" class="modal hide fade" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
        } 
        $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
        $('#dataConfirmOK').attr('href', href);
        $('#dataConfirmModal').modal({show:true});
        return false;
      });
    });
  </script>

  <!-- Membuat Bg Putih -->
  <style type="text/css">
  body {
    /*background: #ffffff;*/
  }
  .card.hovercard {
    background-color: #fff;
    overflow: hidden;
    padding-top: 0;
    position: relative;
    text-align: center;
    width: 700px;
}
.card.hovercard img {
    height: 235px;
    width: 740px;
}

  .btn-success {
    background-color: #047e4f;
    background-image: linear-gradient(to bottom, #047e4f, #047e4f);
    background-repeat: repeat-x;
    border-color: rgba(0, 0, 0, 0.1);
    color: #fff;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  }
  /*.navbar .brand {
    color: #fff;
    display: block;
    float: left;
    font-size: 20px;
    font-weight: 200;
    margin-left: -20px;
    padding: 12px 20px;
    text-shadow: 0 0 #fff;
  }

  .navbar .nav li.dropdown.open > .dropdown-toggle, .navbar .nav li.dropdown.active > .dropdown-toggle, .navbar .nav li.dropdown.open.active > .dropdown-toggle {
    color: #fff;
  }
  .navbar-inner {
    background-color: #357ae8; }

    .navbar .nav > li > a {
      border-bottom: 2px solid transparent;
      color: #fff; }

      .navbar .nav > .active > a, .navbar .nav > .active > a:hover, .navbar .nav > .active > a:focus {
        border-color: #fff;
        box-shadow: none;
        color: #fff;
      }
      .navbar .nav > li > a:hover,
      .navbar .nav > li > a:focus {
        color: #fff;
        border-color: #fff;
      }*/
      .container-narrow {
        margin: 0 auto;
        /*max-width: 1024px;*/
      }


    </style>
  <style type="text/css">
  body { 
    background-color: #fff;
  }
  .card {
        border-width: 0 0px 0px;
  }
  .well {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #fff;
    border-image: none;
    border-left: 0px solid #fff;
    border-radius: 0px;
    border-right: 0px solid #fff;
    border-style: solid;
    border-width: 0 1px 2px;
    box-shadow: none;
}
  </style>
  <script>
                    tinymce.init({selector:'#alamat'});
                    
                    $(function(){
                        $("#tanggal1").datepicker({
                            format:'yyyy-mm-dd'
                        });
                        $("#lapor1").datepicker({
                            hangeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });
                        $("#lapor2").datepicker({
                            hangeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });
                        
                        $("#tanggal2").datepicker({
                            format:'yyyy-mm-dd'
                        });
                        
                        $("#tanggal").datepicker({
                            format:'yyyy-mm-dd'
                        });
                        
                        $( "#tgl_keluar" ).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });
                        $( "#tgl_terima" ).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });
                        $( "#tgl_surat" ).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });
                        
                    })
            </script>
            <script type="text/javascript">
            <?php for ($i=1; $i <=10; $i++) { ?> 
            $(function(){
                    $( "#tgl_start<?php echo $i; ?>" ).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });
                    $( "#tgl<?php echo $i; ?>" ).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });
                    $( "#tgl_keluar<?php echo $i; ?>" ).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: 'yy-mm-dd'
                        });

                    
                     });
            <?php   
            } ?>
            </script>

</head>

<body>