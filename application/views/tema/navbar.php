    <!-- 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/datepicker.css'); ?>">
    <script type="text/javascript" src="<?php echo base_url('asset/datepicker.js'); ?>"></script>
    
    <script>
        $(function(){
            window.prettyPrint && prettyPrint();
            $('.dp4').datepicker({
                format: 'yyyy-mm-dd'
            });
            $('.dp3').datepicker({
                format: 'yyyy-mm-dd'
            });
        });
    </script>
     -->
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="<?php echo site_url(); ?>">System Persuratan</a>
                <div class="nav-collapse">
                    <?php if ($this->session->userdata('LEVEL') == 'admin'){ ?>
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Master<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('perusahaan/data'); ?>"><i class="icon-home"></i> Perusahaan</a></li>
                                <li><a href="<?php echo site_url('divisi/data'); ?>"><i class="icon-group"></i> Divisi</a></li>
                                <li><a href="<?php echo site_url('jenis_surat/data/'); ?>"><i class="icon-list"></i> Jenis Surat</a></li>
                                <li><a href="<?php echo site_url('master/admin');?>"><i class="icon-user"></i> Admin</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('surat_masuk/data'); ?>"><i class="icon-file"></i> Surat Masuk</a></li>
                                <li><a href=""><i class="icon-file"></i> Surat Keluar</a></li>
                                <li><a href=""><i class="icon-book"></i> Input Tanda Terima</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('saya/data'); ?>"><i class="icon-search"></i> Rekap Surat Masuk</a></li>
                                <li><a href="#"><i class="icon-search"></i> Rekap Surat Keluar</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi <?php echo $this->session->userdata('USERNAME'); ?><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url('login/logout'); ?>"><i class="icon-off"></i> Keluar</a></li>
                                </ul>
                            </li>
                        </ul>
                        </div><!-- /.nav-collapse -->
                </div>
            </div><!-- /navbar-inner -->
        </div>
        <br><br><br>
                    <?php } else { ?>
                    <!-- Menu PLP -->
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('saya/data'); ?>"><i class="icon-file"></i> Surat Masuk</a></li>
                                <li><a href="#"><i class="icon-file"></i> Surat Keluar</a></li>
                                <li><a href="">Input Tanda Terima</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('saya/data'); ?>"><i class="icon-search"></i> Rekap Surat Masuk</a></li>
                                <li><a href="#"><i class="icon-search"></i> Rekap Surat Keluar</a></li>
                            </ul>
                        </li>
                    </ul>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi <?php echo $this->session->userdata('USERNAME'); ?><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li></li>
                                    <li><a href="<?php echo site_url('login/logout'); ?>"><i class="icon-off"></i> Keluar</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.nav-collapse -->
                </div>
            </div><!-- /navbar-inner -->
        </div>
        <br><br><br>
         <?php } ?>
