<div class="col-md-9">
<style type="text/css">
    .datepicker{
        z-index: 9999;
    }
</style>
<legend><?php echo $judul; ?></legend>
<a href="<?php echo site_url('surat_keluar/tambah');?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
<div style="float:right">
    <form accept-charset="utf-8" method="post" class="form-search" action="<?php echo base_url('surat_keluar/search'); ?>">    <input type="hidden" name="sifat_surat" value="1"> 
    <input type="text" name="no_surat" class="from-control" placeholder=" Masukan Nomor Surat ">
    <button class="btn btn-primary" type="submit">
        <i class="glyphicon glyphicon-search"></i>
    </button>
</form>
</div>
<br>
<br>
<Table class="table table-bordered table-striped">
    <thead>
        <tr>
            <td>No.</td>
            <td>Nomer Surat</td>
            <td>Tanggal Surat</td>
            <td>Perihal</td>
            <td>Ditujukan</td>
            <td>Sifat Surat</td>
            <td><center>Aksi</center></td>
        </tr>
    </thead>
    <tbody>

    <?php
    $no=1;
    foreach($surat_keluar as $row){
    ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $row['no_surat']; ?></td>
        <td><?php echo $row['tgl']; ?></td>
        <td><?php echo $row['perihal']; ?></td>
        <td><?php echo $row['tujuan'];?></td>
        <td><?php echo $row['sifat_surat']; ?></td>
        <td>   
        <center>
            <a class="btn btn-primary" href="#modalEdit<?php echo $row['id_surat_keluar']?>" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i></a>
            <a class="btn btn-danger" href="<?php echo site_url('surat_keluar/hapus/'.$row['id_surat_keluar']);?>"
               onclick="return confirm('Anda yakin?')"> <i class="glyphicon glyphicon-trash"></i></a>
        </center>
        </td>
    </tr>

    <?php }
    //}
    ?>

    </tbody>
    </Table>
    <?php echo $pagination; ?>
</div>




   <?php
   $no=1;
//if (isset($data_sk)){
    foreach($surat_keluar as $row){
        ?>
<div id="modalEdit<?php echo $row['id_surat_keluar']?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Edit Surat Keluar</h3>
            </div>
<div class="modal-body">
    <form class="form-horizontal" action="<?= base_url()?>surat_keluar/edit_sk" method="post" />
            <input type="hidden" name="id_surat_keluar" value="<?php echo $row['id_surat_keluar']; ?>">
    <div class="form-group">
        <label class="col-lg-3">No Agenda</label>
        <div class="col-lg-3">
            <input type="text" name="no_agenda" class="form-control" value="<?php echo $row['no_agenda']; ?>">
        </div>
         <label class="col-lg-3">Tanggal</label>
        <div class="col-lg-3">
            <input type="text" name="tgl" class="datepicker form-control" id="tgl_keluar<?php echo $no; ?>" value="<?php echo $row['tgl']; ?>" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3">Nomor surat</label>
        <div class="col-lg-3">
            <input type="text" name="no_surat" class="form-control" value="<?php echo $row['no_surat']; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-lg-3">Tujuan Surat</label>
        <div class="col-lg-5">
            <input type="text" name="tujuan" class="form-control" value="<?php echo $row['tujuan']; ?>">
        </div> 
    </div>
    <div class="form-group">
        <label class="col-lg-3">Perihal</label>
        <div class="col-lg-9">
            <textarea name="perihal" value=""><?php echo $row['perihal']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3">Sifat Surat</label>
        <div class="col-lg-5">
            <select class="from-control" style="width:40%" name="sifat_surat">
                <option value="Biasa">Biasa</option>
                <option value="Undangan">Undangan</option>
                <option value="Pengantar">Pengantar</option>
                <option value="Rahasia">Rahasia</option>
                <option value="Arsip">Arsip</option>
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary"><i class="glyphicon glyphicon-hdd"></i> Update</button>
        <a href="<?php echo site_url('surat_keluar');?>" class="btn btn-default">Kembali</a>
    </div>
</form>
</div>
</div>
</div>
</div>
        <?php  $no++; }
//}
?>