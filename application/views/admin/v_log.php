<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">

  <title><?php echo $title; ?></title>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css">

</head>

<body>

  <div class="container">

  <div id="login-form">

    <h3>Login</h3>

    <fieldset>

<?php echo form_open('login/cek_login');?>
      <?php
            $message = $this->session->flashdata('notif');
            if($message){
                echo '<p class="alert alert-danger text-center">'.$message .'</p>';
            }?>

        <input type="text" required  name="kd_user"><!-- JS because of IE support; better: placeholder="Email" -->

        <input type="password" required name="password"> <!-- JS because of IE support; better: placeholder="Password" -->

        <!-- <input type="submit" value="Login">  -->
        <button class="btn btn-primary">Login</button>

        <footer class="clearfix">

          <!-- <p><span class="info">?</span><a href="#">Forgot Password</a></p> -->

        </footer>

      </form>

    </fieldset>

  </div> <!-- end login-form -->

</div>

</body>

</html>