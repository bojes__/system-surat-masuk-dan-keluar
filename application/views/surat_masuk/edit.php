<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<div class="col-md-9">
<legend><?php echo $judul;?></legend>
<form class="form-horizontal" action="<?= base_url()?>surat_masuk/save_suratmasuk" method="post" enctype="multipart/form-data" />
   <!--  <?php echo validation_errors(); echo $message;?> -->
   <?php
if (isset($data_sm)){
    foreach($data_sm as $row){
        ?>
    <?php }
}
?>
    <div class="form-group">
        <label class="col-lg-2">No Agenda</label>
        <div class="col-lg-3">
            <input type="text" name="no_agenda" class="form-control" value="<?php echo $row->no_agenda; ?>">
        </div>
         <label class="col-lg-2">Tanggal Diterima</label>
        <div class="col-lg-3">
            <input type="text" name="tgl_diterima" class="form-control" id="tgl_start" value="<?php echo $row->tgl_diterima; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-lg-2">Nama Pengirim</label>
        <div class="col-lg-5">
            <input type="text" name="nama_pengirim" class="form-control" value="<?php echo $row->nama_pengirim; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-lg-2">No surat</label>
        <div class="col-lg-3">
            <input type="text" name="no_surat" class="form-control" value="<?php echo $row->no_surat; ?>">
        </div>
        <label class="col-lg-2">Tanggal Surat</label>
        <div class="col-lg-3">
            <input type="text" name="tgl_surat" class="form-control" id="tgl" value="<?php echo $row->tgl_surat; ?>">
        </div>
    </div>
    
    <div class="form-group">
        
    </div>
    <div class="form-group">
        <label class="col-lg-2">Perihal</label>
        <div class="col-lg-10">
            <textarea name="perihal"></textarea>
            <input value="<?php echo $row->perihal;?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2">Sifat Surat</label>
        <div class="col-lg-5">
            <select class="from-control" style="width:40%">
                <option>Biasa</option>
                <option>Undangan</option>
                <option>Pengantar</option>
                <option>Rahasia</option>
                <option>Arsip</option>
            </select>
        </div>
    </div>
    <div class="form-group well">
        <button class="btn btn-primary"><i class="glyphicon glyphicon-hdd"></i> Update</button>
        <a href="<?php echo site_url('surat_masuk');?>" class="btn btn-primary">Kembali</a>

    </div>
</form>