<div class="col-md-9">
<legend><?php echo $judul; ?></legend>
<a href="<?php echo site_url('surat_masuk/tambah');?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
<br>
<br>
<Table class="table table-bordered table-striped">
    <thead>
        <tr>
            <td>No.</td>
            <td>Nomer Surat</td>
            <td>Tanggal Surat</td>
            <td>Tanggal Terima</td>
            <td>Perihal</td>
            <td>Pengirim</td>
            <td>Sifat Surat</td>
            <td><center>Aksi</center></td>
        </tr>
    </thead>
    <tbody>

    <?php
    $no=1;
    //if(isset($data_siswa)){
    foreach($data_siswa as $row){
    ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $row->no_surat; ?></td>
        <td><?php echo $row->tgl_surat; ?></td>
        <td><?php echo $row->tgl_diterima; ?></td>
        <td><?php echo $row->perihal;?></td>
        <td><?php echo $row->nama_pengirim;?></td>
        <td><?php echo $row->sifat_surat; ?></td>
        <td>   
        <center>
            <a class="btn btn-primary" href="#modalEdit<?php echo $row->id_surat_masuk?>" data-toggle="modal"><i class="glyphicon glyphicon-edit"></i></a>
            <a class="btn btn-danger" href="<?php echo site_url('surat_masuk/hapus/'.$row->id_surat_masuk);?>"
               onclick="return confirm('Anda yakin?')"> <i class="glyphicon glyphicon-trash"></i></a>
        </center>
        </td>
    </tr>

    <?php }
    //}
    ?>

    </tbody>
    </Table>
    <?php echo $halaman?>
</div>




   <?php
if (isset($data_sk)){
    foreach($data_sk as $row){
        ?>
<div id="modalEdit<?php echo $row->id_surat_masuk?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Edit Surat Masuk</h3>
            </div>
<div class="modal-body">
    <form class="form-horizontal" action="<?= base_url()?>surat_masuk/edit" method="post" />
            <input type="hidden" name="id_surat_masuk" value="<?php echo $row->id_surat_masuk; ?>">
    <div class="form-group">
        <label class="col-lg-3">No Agenda</label>
        <div class="col-lg-3">
            <input type="text" name="no_agenda" class="form-control" value="<?php echo $row->no_agenda; ?>">
        </div>
         <label class="col-lg-3">Tanggal Diterima</label>
        <div class="col-lg-3">
            <input type="text" name="tgl_diterima" class="form-control" id="tgl_start" value="<?php echo $row->tgl_diterima; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-lg-3">Nama Pengirim</label>
        <div class="col-lg-5">
            <input type="text" name="nama_pengirim" class="form-control" value="<?php echo $row->nama_pengirim; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-lg-3">No surat</label>
        <div class="col-lg-3">
            <input type="text" name="no_surat" class="form-control" value="<?php echo $row->no_surat; ?>">
        </div>
        <label class="col-lg-3">Tanggal Surat</label>
        <div class="col-lg-3">
            <input type="text" name="tgl_surat" class="form-control" id="tgl" value="<?php echo $row->tgl_surat; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3">Perihal</label>
        <div class="col-lg-9">
            <textarea name="perihal"><?php echo $row->perihal; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3">Sifat Surat</label>
        <div class="col-lg-5">
            <select class="from-control" style="width:40%" name="sifat_surat">
                <option value="Biasa">Biasa</option>
                <option value="Undangan">Undangan</option>
                <option value="Pengantar">Pengantar</option>
                <option value="Rahasia">Rahasia</option>
                <option value="Arsip">Arsip</option>
            </select>
        </div>
    </div>
    <div class="">
        <button class="btn btn-primary"><i class="glyphicon glyphicon-hdd"></i> Update</button>
        <a href="<?php echo site_url('surat_masuk');?>" class="btn btn-primary">Kembali</a>
    </div>
</form>
</div>
</div>
</div>
</div>
        <?php }
}
?>