<div class="container">
<?php if ($this->session->userdata('LEVEL') == 'admin'){ ?>
                <div class="row">
                    <div class="col-md-3">
<div class="panel-group" id="accordion">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-cog text-danger">
                                        </span> Master</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <table class="table">
                                        <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil text-primary"></span> <a href="<?php echo site_url('anggota');?>"> Perusahaan</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil text-primary"></span> <a href="<?php echo site_url('anggota');?>"> Divisi</a>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo site_url('master/kop_surat');?>"> Jenis Surat</a>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-user"></span> <a href="<?php echo site_url('master/petugas');?>"> Admin</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-book text-danger">
                            </span> Transaksi</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="<?php echo site_url('surat_masuk');?>"> Surat Masuk</a></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <span class="glyphicon glyphicon-tasks"></span> <a href="<?php echo site_url('surat_keluar');?>"> Surat Keluar</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file text-danger">
                            </span> Report</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list-alt"></span><a href="<?php echo site_url('surat_masuk/laporan_suratmasuk');?>"> Surat Masuk </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list-alt"></span><a href="<?php echo site_url('surat_keluar/laporan_suratkeluar');?>"> Surat Keluar</a>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="<?php echo site_url('laporan/peminjaman');?>"> Data Peminjaman</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list-alt"></span><a href="<?php echo site_url('laporan/pengembalian');?>"> Data Pengembalian</a>
                                    </td>
                                </tr> -->
                            </table>
                        </div>
                    </div>
                </div>
                
                <!-- <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="<?php echo site_url('');?>"><span class="glyphicon glyphicon-off">
                            </span> Logout</a>
                        </h4>
                    </div>
                </div> -->
</div>
</div>
<?php } else { ?>
 <div class="row">
                    <div class="col-md-3">
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-book text-danger">
                            </span> Persuratan</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="<?php echo site_url('surat_masuk');?>"> Surat Masuk</a></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <span class="glyphicon glyphicon-tasks"></span> <a href="<?php echo site_url('surat_keluar');?>"> Surat Keluar</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file text-danger">
                            </span> Laporan</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list-alt"></span><a href="<?php echo site_url('surat_masuk/laporan_suratmasuk');?>"> Surat Masuk </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list-alt"></span><a href="<?php echo site_url('surat_keluar/laporan_suratkeluar');?>"> Surat Keluar</a>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-tasks"></span><a href="<?php echo site_url('laporan/peminjaman');?>"> Data Peminjaman</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-list-alt"></span><a href="<?php echo site_url('laporan/pengembalian');?>"> Data Pengembalian</a>
                                    </td>
                                </tr> -->
                            </table>
                        </div>
                    </div>
                </div>
                
                <!-- <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="<?php echo site_url('');?>"><span class="glyphicon glyphicon-off">
                            </span> Logout</a>
                        </h4>
                    </div>
                </div> -->
</div>
</div>
<?php } ?>