<div class=" container">
<!-- <div class="col-md-9"> -->
<legend><?php echo $judul;?></legend>
<div style="float:right">
    <form accept-charset="utf-8" method="post" class="form-search" action="<?php echo base_url('master/search'); ?>">    <input type="hidden" name="sifat_surat" value="1"> 
    <input type="text" name="username" class="from-control" placeholder=" Masukan User Id ">
    <button class="btn btn-primary" type="submit">
        <i class="icon-search"></i>
    </button>
</form>
</div>
<br>
<br>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>No</th>
        <!-- <th>Kode Pegawai</th> -->
        <th>Id Karyawan</th>
        <th>Nama Pegawai</th>
        <th>Level</th>
        <th class="span4">
        <center>
            <a href="<?php echo site_url('master/tambah'); ?>" class="btn btn-primary">
                <i class="icon-plus"></i> Tambah Data
            </a>
        </center>

<!--            <a href="#" class="btn btn-mini btn-block btn-inverse disabled" data-toggle="modal">-->
<!--                <i class="icon-plus-sign icon-white"></i> Tambah Data-->
<!--            </a>-->
        </th>
    </tr>
    </thead>
    <tbody>

    <?php
    $no=1; foreach($data_pegawai as $row):
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <!-- <td><?php echo $row['kd_user']; ?></td> -->
                <td><?php echo $row['username']; ?></td>
                <td><?php echo $row['nama']; ?></td>
                <td><?php echo $row['level']; ?></td>

                <td>
                <center> 
                    <a class="btn btn-info" href="#modalEditPegawai<?php echo $row['kd_user']?>" data-toggle="modal"><i class="icon-edit"></i> Edit</a>
                    <a class="btn btn-danger" href="<?php echo site_url('master/hapus_pegawai/'.$row['kd_user']);?>"
                       data-confirm="Apakah Anda Ingin menghapusnya ? "> <i class="icon-trash"></i> Hapus</a>
                    <a href="<?php echo site_url('master/edit/'.$row['kd_user']); ?>">anu</a>

<!--                    <a class="btn btn-mini disabled" href="#" data-toggle="modal"><i class="icon-pencil"></i> Edit</a>-->
<!--                    <a class="btn btn-mini disabled" href="#"> <i class="icon-remove"></i> Hapus</a>-->
                </center>
                </td>

            </tr>

        <?php $no++; endforeach ?>

    </tbody>
</table>
<?php echo $halaman; ?>


<!-- ============ MODAL ADD PEGAWAI =============== 
<div id="modalAddPegawai" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel">Tambah Data Pegawai</h3>
    </div>
    <form class="form-horizontal" method="post" action="<?php echo site_url('master/tambah_pegawai')?>">
        <div class="modal-body">
            <div class="from-group">
                <label class="control-label">Kode Pegawai</label>
                <div class="controls">
                    <input name="kd_user" type="text" value="<?php echo $kd_user; ?>" readonly>
                </div>
            </div>

            <div class="from-group">
                <label class="control-label" >User ID</label>
                <div class="controls">
                    <input name="username" type="text" required>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" >Password</label>
                <div class="controls">
                    <input name="password" type="password" required>
                </div>
            </div>

            <hr/>

            <div class="control-group">
                <label class="control-label">Nama Pegawai</label>
                <div class="controls">
                    <input name="nama" type="text">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Level</label>
                <div class="controls">
                    <select name="level" id="level">
                        <option value=""> = Pilih Level Akses = </option>
                        <option value="user">User</option>
                        <option value="admin">Admin</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary">Save</button>
        </div>
    </form>
</div>
</div>
</div>

-->

<!-- ============ MODAL EDIT PEGAWAI =============== 
-->
<?php
//if (isset($data_pegawai)){
    foreach($data_pegawai as $row){
        ?>
            <div id="modalEditPegawai<?php echo $row['kd_user']?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                    <h3 id="myModalLabel">Edit Pegawai</h3>
                </div>

            <form class="form-horizontal" method="post" action="<?php echo site_url('master/edit_pegawai')?>">
                <div class="modal-body">
                    <div class="control-group">
                        <!-- <label class="control-label">Kode Pegawai</label> -->
                        <div class="controls">
                            <input name="kd_user" type="hidden" value="<?php echo $row['kd_user']; ?>" class="form-control" readonly="true">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" >ID Karyawan</label>
                        <div class="controls">
                            <input name="username" type="text" value="<?php echo $row['username']?>" required class="form-control">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" >Password</label>
                        <div class="controls">
                            <input name="password" type="password" required class="form-control">
                        </div>
                    </div>

                    <hr/>

                    <div class="control-group">
                        <label class="control-label">Nama Pegawai</label>
                        <div class="controls">
                            <input name="nama" type="text" value="<?php echo $row['nama']?>" class="form-control">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Level</label>
                        <div class="controls">
                            <select name="level" id="level" class="form-control">
                                <?php if($row['level'] == 'admin'){?>
                                    <option value="admin" selected="selected">Admin</option>
                                <?php }else{ ?>
                                    <option value="user">User</option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        </div>
        </div>
    <?php }
// }
?>