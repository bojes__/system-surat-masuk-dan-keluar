<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<div class="container">
<legend><?php echo $judul;?>
<div style="float:right">
    <a href="<?php echo site_url('master/admin');?>" class="btn btn-primary">Kembali</a>
</div>
</legend>
<form class="form-horizontal" method="post" action="<?php echo site_url('master/tambah_pegawai')?>">
        <!-- <div class="modal-body"> -->
                    <input name="kd_user" type="hidden" value="<?php echo $kd_user; ?>" readonly class="form-control"><br>
            <div class="control-group">
                <label class="control-label">Id Karayawan</label>
                <div class="controls">
                    <input name="username" type="text" required class="form-control">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Nama</label>
                <div class="controls">
                    <input name="nama" type="text" required class="form-control">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Alamat</label>
                <div class="controls">
                    <!-- <input name="nama" type="text" class="form-control"> -->
                    <textarea name="alamat" rows="5" required></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Divisi</label>
                    <div class="controls">
                        <input type="text" name="divisi" required class="form-control">
                    </div>
            </div>
            <div class="control-group">
                <label class="control-label">Jabatan</label>
                    <div class="controls">
                        <input type="text" name="jabatan" required class="form-control">
                    </div>
            </div>
            <div class="control-group">
                <label class="control-label">No. Tlp</label>
                    <div class="controls">
                        <input type="text" name="tlp" required class="form-control">
                    </div>
            </div>

            <div class="control-group">
                <label class="control-label" >Password</label>
                <div class="controls">
                    <input name="password" type="password" required class="form-control">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Level</label>
                <div class="controls">
                    <select name="level" id="level" class="form-control">
                        <option value=""> = Pilih Level Akses = </option>
                        <option value="user">User</option>
                        <option value="admin">Admin</option>
                    </select>
                </div>
            </div>
        <!-- </div> -->

        <div class="footer">
            <!-- <button class="btn" >Close</button> -->
            <button class="btn btn-primary">Simpan</button>
        </div>
    </form>
    </div>