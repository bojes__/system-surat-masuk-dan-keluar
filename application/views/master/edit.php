
<div class="container">
<legend><?php echo $judul;?></legend>
<?php
//if (isset($data_pegawai)){
    foreach($data_pegawai as $row){
        ?>
<form class="form-horizontal" method="post" action="<?php echo site_url('master/edit_pegawai')?>">
                <!-- <div class="modal-body"> -->
                    <div class="control-group">
                        <label class="control-label">Kode Pegawai</label>
                        <div class="controls">
                            <input name="kd_user" type="text" value="<?php echo $row['kd_user']; ?>" class="form-control" readonly="true">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" >User ID</label>
                        <div class="controls">
                            <input name="username" type="text" value="<?php echo $row['username']?>" required class="form-control">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" >Password</label>
                        <div class="controls">
                            <input name="password" type="password" required class="form-control">
                        </div>
                    </div>

                    <hr/>

                    <div class="control-group">
                        <label class="control-label">Nama Pegawai</label>
                        <div class="controls">
                            <input name="nama" type="text" value="<?php echo $row['nama']?>" class="form-control">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Level</label>
                        <div class="controls">
                            <select name="level" id="level" class="form-control">
                                <?php if($row['level'] == 'admin'){?>
                                    <option value="admin" selected="selected">Admin</option>
                                <?php }else{ ?>
                                    <option value="user">User</option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                <!-- </div> -->

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </form>
    <?php }
// }
?>
</div>