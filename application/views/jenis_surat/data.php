<div class="container">
<legend><?php echo $judul; ?></legend>
<div style="float:right">
<form accept-charset="utf-8" method="post" class="form-search" action="<?php echo base_url('master/search'); ?>"> 
    <input type="text" name="username" class="from-control" placeholder=" Masukan Id ">
    <button class="btn btn-primary" type="submit">
        <i class="icon-search"></i>
    </button>
</form>
</div>
<p>
<a href="#myModal" role="button" data-toggle="modal" class="btn btn-primary"><i class="icon-plus"></i> Tambah Data</a></p>
  
<table class="table table-bordered">
	<th>No</th>
  <th>id</th>
	<th>Tipe urat</th>
  <th>Skala Prioritas</th>
	<th>Action</th>
	<?php $no = 1; foreach ($data as $row) {
		 ?>

	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $row['id_jenis']; ?></td>
		<td><?php echo $row['tipe_surat']; ?></td>
    <td><?php echo $row['skala_p']; ?></td>
		<td>
			<div class="btn-group">
				<!-- <a href="" class="btn"><i class="icon-eye-open"></i></a> -->
				<a href="#edit_modal<?php echo $row['id_jenis']; ?>" data-toggle="modal" class="btn"><i class="icon-pencil"></i></a>
				<a href="<?php echo site_url('jenis_surat/hapus/'.$row['id_jenis']); ?>" data-confirm="Apakah anda ingin menghapusnya ?"  class="btn btn-danger"><i class="icon-trash"></i></a>
			</div>
		</td>
	</tr>
	<?php $no++; } ?>
</table>
	<div class="halaman">Halaman : <?php echo $halaman;?></div>
</div>
 
<!-- Modal Tambah-->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Tambah Jenis Surat</h3>
  </div>
  <div class="modal-body">
  <form action="<?= base_url()?>jenis_surat/add_jenis_surat" method="post" enctype="multipart/form-data">
  	<table>	
    	<tr>
    		<!-- <input type="hidden" name="id_jenis_surat"> -->
    		<td>Tipe Surat</td>
    		<td>:</td>
    		<td><input type="text" class="input-blok-level" name="tipe_surat"></td>
    	</tr>
      <tr>
        <!-- <input type="hidden" name="id_jenis_surat"> -->
        <td>Skala Prioritas</td>
        <td>:</td>
        <td><input type="text" class="input-blok-level" name="skala_p"></td>
      </tr>
  	</table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save Data</button>
  </form>
  </div>
</div>
<!-- end modal tambah -->

<!-- modal edit -->
  	<?php foreach ($data as $key) {
     ?>
<div id="edit_modal<?php echo $key['id_jenis']; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Edit Jenis Surat</h3>
  </div>
  <div class="modal-body">
  <!-- <form action="<?= base_url()?>jenis_surat/add_jenis_surat" method="post" enctype="multipart/form-data"> -->
   <?php echo form_open('jenis_surat/edit'); ?>
    <table>
      <tr>
    		<input type="hidden" name="id_jenis" value="<?php echo $key['id_jenis']; ?>">
        <td>Tipe Surat</td>
        <td>:</td>
        <td><input type="text" class="input-blok-level" name="tipe_surat" value="<?php echo $key['tipe_surat']; ?>"></td>
      </tr>
      <tr>
        <td>Skala Prioritas</td>
        <td>:</td>
        <td><input type="text" class="input-blok-level" name="skala_p" value="<?php echo $key['skala_p']; ?>"></td>
      </tr>
    </table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save Data</button>
  </form>
  </div>
</div>
    	<?php  } ?>
<!-- end modal edit -->