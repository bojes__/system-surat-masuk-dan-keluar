<div class="container">
<legend><?php echo $judul; ?></legend>
<p>
<a href="#myModal" role="button" data-toggle="modal" class="btn btn-primary"><i class="icon-plus"></i> Tambah Data</a></p>
<table class="table table-bordered">
	<th>No</th>
	<th>Kode</th>
	<th>Jenis Surat</th>
  <th>Kepala Divisi</th>
	<th>Action</th>
	<?php $no = 1; foreach ($data as $row) {
		 ?>

	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $row['id_divisi']; ?></td>
		<td><?php echo $row['nama_divisi']; ?></td>
    <td><?php echo $row['kepala_divisi']; ?></td>
		<td>
			<div class="btn-group">
				<!-- <a href="" class="btn"><i class="icon-eye-open"></i></a> -->
				<a href="#edit_modal<?php echo $row['id_divisi']; ?>" data-toggle="modal" class="btn"><i class="icon-pencil"></i></a>
				<a href="<?php echo site_url('divisi/hapus/'.$row['id_divisi']); ?>" data-confirm="Apakah anda ingin menghapusnya ?"  class="btn btn-danger"><i class="icon-trash"></i></a>
			</div>
		</td>
	</tr>
	<?php $no++; } ?>
</table>
	<div class="halaman">Halaman : <?php echo $halaman;?></div>
</div>

<!-- Button to trigger modal -->
<!-- <a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a> -->
 
<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Tambah Divisi</h3>
  </div>
  <div class="modal-body">
  <form action="<?= base_url()?>divisi/add" method="post" enctype="multipart/form-data">
  	<table class="table-hover">	
    	<tr>
    		<input type="hidden" name="id_divisi">
    		<td>Nama Divisi</td>
    		<td>:</td>
    		<td><input type="text" class="input-blok-level" name="nama_divisi"></td>
    	</tr>
      <tr>
        <td>Kepala Divisi</td>
        <td>:</td>
        <td><input type="text" class="input-blok-level" name="kepala_divisi"></td>
      </tr>
  	</table>
    <!-- <input type="hidden" name="id_divisi">
    <div class="control-group">
      <div class="controls"> 
        <td>Nama Divisi</td>
        <td>:</td>
        <td><input type="text" class="input-blok-level" name="nama_divisi"></td>
      </div>
    </div>
    <div class="control-group">
      <div class="controls"> 
        <td>Kepala Divisi</td>
        <td>:</td>
        <td><input type="text" class="input-blok-level" name="kepala_divisi"></td>
      </div> -->
    </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save Data</button>
  </form>
  </div>
</div>
<!-- modal edit -->
  	<?php foreach ($data as $key) {
  	 ?>
<div id="edit_modal<?php echo $key['id_divisi']; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Edit Divisi</h3>
  </div>
  <div class="modal-body">
  <!-- <form action="<?= base_url()?>" method="post" enctype="multipart/form-data"> -->
  <?php echo form_open('divisi/edit'); ?>
    <table> 
    	<tr>
    		<input type="hidden" name="id_divisi" value="<?php echo $key['id_divisi']; ?>">
    		<td>Nama Divisi</td>
    		<td>:</td>
    		<td><input type="text" class="input-blok-level" name="nama_divisi" value="<?php echo $key['nama_divisi']; ?>"></td>
    	</tr>
      <tr>
        <td>Nama Divisi</td>
        <td>:</td>
        <td><input type="text" class="input-blok-level" name="kepala_divisi" value="<?php echo $key['kepala_divisi']; ?>"></td>
      </tr>
    </table>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save Data</button>
  </form>
  </div>
</div>
    	<?php  } ?>