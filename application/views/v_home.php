<style>
    .glyphicons {
        padding-left: 0;
        padding-bottom: 1px;
        margin-bottom: 20px;
        list-style: none;
        overflow: hidden;
      }
          
      .glyphicons li {
        float: left;
        width: 11.5%;
        height: 115px;
        padding: 10px;
        margin: 0 -1px -1px 0;
        font-size: 12px;
        line-height: 1.4;
        text-align: center;
        border: 1px solid #ddd;
      }
      
      .glyphicons .glyphicon {
              margin-top: 5px;
              margin-bottom: 10px;
              font-size: 24px;
          display: block;
              text-align: center;
      }
</style>
<div class="container">
  
<?php if ($this->session->userdata('LEVEL') == 'admin'){ ?>
<div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading">
        Selamat Datang <?php echo $this->session->userdata('NAME'); ?> Pengelolaan
    </div>
    <div class="panel-body">
        <div class="container">
            <ul class="glyphicons">
                <li>
                  <span class="glyphicon glyphicon-user"></span>
                  <a href="<?php echo site_url('master/petugas');?>">Petugas</a>
                </li>
                <!-- <li>
                  <span class="glyphicon glyphicon-list-alt"></span>
                  <a href="<?php echo site_url('master/kop_surat');?>">Kop Surat</a>
                </li> -->
                
                <li>
                  <span class="glyphicon glyphicon-tasks"></span>
                  <a href="<?php echo site_url('surat_masuk');?>">Surat Masuk</a>
                </li>
                
                <li>
                  <span class="glyphicon glyphicon-tasks"></span>
                  <a href="<?php echo site_url('surat_keluar');?>">Surat Keluar</a>
                </li>
                
                <li>
                  <span class="glyphicon glyphicon-print"></span>
                  <a href="<?php echo site_url('surat_masuk/laporan_suratmasuk');?>">Laporan Surat Masuk</a>
                </li>
                
                <li>
                  <span class="glyphicon glyphicon-print"></span>
                  <a href="<?php echo site_url('buku');?>">Laporan Surat Keluar</a>
                </li>
                
                <!-- <li>
                  <span class="glyphicon glyphicon-off"></span>
                  <a href="<?php echo site_url('dashboard/logout');?>">Logout</a>


                </li> -->
            </ul>
        </div>

    </div>
</div>
</div>
</div>
</div>
</div>
<?php } else { ?>
<div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading">
        Selamat Datang <?php echo $this->session->userdata('NAME'); ?> Pengelolaan
    </div>
    <div class="panel-body">
        <div class="container">
            <ul class="glyphicons">
                <!-- <li>
                  <span class="glyphicon glyphicon-user"></span>
                  <a href="<?php echo site_url('master/petugas');?>">Petugas</a>
                </li> -->
                
                <li>
                  <span class="glyphicon glyphicon-tasks"></span>
                  <a href="<?php echo site_url('surat_masuk');?>">Surat Masuk</a>
                </li>
                
                <li>
                  <span class="glyphicon glyphicon-tasks"></span>
                  <a href="<?php echo site_url('surat_keluar');?>">Surat Keluar</a>
                </li>
                
                <li>
                  <span class="glyphicon glyphicon-print"></span>
                  <a href="<?php echo site_url('surat_masuk/laporan_suratmasuk');?>">Laporan Surat Masuk</a>
                </li>
                
                <li>
                  <span class="glyphicon glyphicon-print"></span>
                  <a href="<?php echo site_url('surat_keluar/laporan_suratkeluar');?>">Laporan Surat Keluar</a>
                </li>
                
                <!-- <li>
                  <span class="glyphicon glyphicon-off"></span>
                  <a href="<?php echo site_url('dashboard/logout');?>">Logout</a>


                </li> -->
            </ul>
        </div>

    </div>
</div>
</div>
<?php } ?>