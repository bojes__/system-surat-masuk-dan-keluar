<?php 
/**
* 
*/
class Perusahaan extends CI_Controller
{
	
	function __construct()
	{
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
        $this->load->model('model_app');
        $this->load->library(array('pagination'));
        //$this->load->helper('currency_format_helper');
		$this->load->model('perusahaan_model','pm',true);
	}

	public function index()
	{
		$this->data();
	}
	public function data($id=null)
	{
		$count = count($this->pm->itung_get_data());
		$config['base_url'] = base_url("peusahaan/data");// base_url nya belum bisa
		$config['total_rows'] = $count; 
		$config['per_page'] = '2';
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		//inisialisasi config
		$this->pagination->initialize($config);
		$data = array(
			'title' => 'Perusahaan',
			'judul' => 'Master Perusahaan',
			// 'das'
			'data' => $this->pm->get_data($config['per_page'],$id),
			// 'headerGrup' => 'Grafik berdasarkan Grup Hak Akses',
			'halaman' => $this->pagination->create_links(),
			);	
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		$this->load->view('perusahaan/data', $data);
	}
	public function add()
	{
		$item = array(
			'id_perusahaan' => $this->pm->get_kode_perusahaan('') ,
			'nama' => $this->input->post('nama'),
			'alamat' =>$this->input->post('alamat'),
			'kota' =>$this->input->post('kota'),
			'tlp' =>$this->input->post('tlp'), );
		$this->pm->insertData('tbl_perusahaan',$item);
        redirect("perusahaan");
	}
	public function hapus()
	{
        $id['id_perusahaan'] = $this->uri->segment(3);
        $this->pm->deleteData('tbl_perusahaan',$id);
        redirect("perusahaan");
	}
	public function edit()
	{
		$id['id_perusahaan'] = $this->input->post('id_perusahaan');
		$data = array(
			// 'id_divisi' => $this->input->post('id_divisi') ,
			'nama' => $this->input->post('nama'),
			'alamat' =>$this->input->post('alamat'),
			'kota' =>$this->input->post('kota'),
			'tlp' =>$this->input->post('tlp'),);

		$this->dm->updateData('tbl_perusahaan',$data,$id);
		redirect("perusahaan");
	}
}

/* End of file perusahaan.php */
/* Location: ./application/controllers/perusahaan.php */
 ?>