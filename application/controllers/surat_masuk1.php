<?php 
/**
* 
*/
class Surat_masuk extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
		//$this->load->model('m_user');
		$this->load->model('model_surat_masuk','sm',true);
		$this->load->helper(array('url', 'form'));
	}

	//lihat data surat
	function index($offset=null)
	{
		$jml = $this->db->get('tbl_surat_masuk');
            
            $config['base_url'] = base_url().'surat_masuk/index';
            
            $config['total_rows'] = $jml->num_rows();
            $config['per_page'] = 5; /*Jumlah data yang dipanggil perhalaman*/  
            $config['uri_segment'] = 3; /*data selanjutnya di parse diurisegmen 3*/
            
            /*Class bootstrap pagination yang digunakan*/
            $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";
        
            $this->pagination->initialize($config);

		$data = array(
			'title' =>'Persuratan' ,
			'grup' => $this->sm->get_master_surat($config['per_page'],$offset),
			//'data_sk'=>$this->sm->getAllData('tbl_surat_masuk'),
			'offset'=>$offset,
			'pagination' => $this->pagination->create_links(),
			'judul'=>'Data Surat Masuk' );
		$this->load->view('template',$data);
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('surat_masuk/index',$data);
	}
	//end

	//cari surat
	public function search($value='')
	{
		//$this->load->model('dev_auth_model');
		$value = $this->input->post('no_surat');
		$data = array(
			'title' => 'Hasil Pencarian',
			'judul' => 'Data Surat Masuk',
			'data' => $this->sm->search_masuk($value), );
		$this->load->view('template',$data);
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('surat_masuk/search',$data);
	}
	//end

	//tambah surat
	public function tambah()
	{
		$data = array('title' =>'Persuratan' ,
		'judul'=>'Input Surat Masuk' );
		$this->load->view('template',$data);
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('surat_masuk/tambah',$data);
	}
	//end

	//edit surat
	public function edit()
	{
		$id['id_surat_masuk'] = $this->input->post('id_surat_masuk');
		$data = array(
			//'id_surat_masuk' =>$this->sm->getKode_acak_suratmasuk() ,
			'no_agenda'=>$this->input->post('no_agenda'),
			'tgl_diterima'=>$this->input->post('tgl_diterima'),
			'nama_pengirim'=>$this->input->post('nama_pengirim'),
			'no_surat'=>$this->input->post('no_surat'),
			'tgl_surat'=>$this->input->post('tgl_surat'),
			'perihal'=>$this->input->post('perihal'),
			'sifat_surat'=>$this->input->post('sifat_surat') );

		$this->sm->updateData('tbl_surat_masuk',$data,$id);
        redirect("surat_masuk");	
	}
	//end

	//simpan surat
	public function save()
	{
		$data = array(
			'id_surat_masuk' =>$this->sm->getKode_acak_suratmasuk() ,
			'no_agenda'=>$this->input->post('no_agenda'),
			'tgl_diterima'=>$this->input->post('tgl_diterima'),
			'nama_pengirim'=>$this->input->post('nama_pengirim'),
			'no_surat'=>$this->input->post('no_surat'),
			'tgl_surat'=>$this->input->post('tgl_surat'),
			'perihal'=>$this->input->post('perihal'),
			'sifat_surat'=>$this->input->post('sifat_surat') );

		$this->sm->insertData('tbl_surat_masuk',$data);
        redirect("surat_masuk");
	}
	//end


	//hapus surat
	public function hapus(){
        $id['id_surat_masuk'] = $this->uri->segment(3);
        $this->sm->deleteData('tbl_surat_masuk',$id);
        redirect("surat_masuk");
    }
    //end

    //laporan
    public function laporan_suratmasuk()
	{
		$data = array('title' =>'Laporan' ,
		'judul'=>'Laporan Surat Masuk' );
		$this->load->view('template',$data);
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('surat_masuk/laporan/index',$data);
	}
	//end

	//cetak surat
	public function laporan()
	{
		//$this->load->model('dev_auth_model');
		// $value = $this->input->post('tgl_surat');
		$value = $this->input->post('sifat_surat');
		$anu = $this->input->post('optionsRadios');
		if (empty($value) || $value == 'null' || $anu=='all') {

		$data = array(
			'title' => 'Cetak Surat Masuk',
			'judul' => 'AGENDA SURAT MASUK',
			'data' => $this->sm->cetak_masuk($anu), );
		$this->load->view('template',$data);
		//$this->load->view('template/header',$data);
		//$this->load->view('template/sidebar',$data);
		$this->load->view('surat_masuk/laporan/laporan',$data);
		} else {

		$data = array(
			'title' => 'Cetak Surat Masuk',
			'judul' => 'AGENDA SURAT MASUK',
			'data' => $this->sm->cetak_masuk($value), );
		$this->load->view('template',$data);
		//$this->load->view('template/header',$data);
		//$this->load->view('template/sidebar',$data);
		$this->load->view('surat_masuk/laporan/laporan',$data);
		}
	}
	//end
}

 ?>