<?php 
/**
* 
*/
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
        $this->load->model('model_app');
        
        
    }

    //halaman depan
	public function index()
	{
		$data = array('title' =>'Home' ,
		'kd_pegawai'=>$this->model_app->getKodePegawai(),
		'data_pegawai'=>$this->model_app->getAllData('tbl_user'), );
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		// $this->load->view('template/sidebar',$data);
		$this->load->view('jajal',$data);
		//$this->load->view('template/v_footer');
	}
	//end halaman depan
}

 ?>