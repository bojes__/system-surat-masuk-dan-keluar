<?php 
/**
* 
*/
class Master extends CI_Controller
{
	
	function __construct(){
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
        $this->load->model('model_app');
        $this->load->library(array('pagination'));
        //$this->load->helper('currency_format_helper');
    }
    //melihat data petugas dengan pagination
    public function admin($offset=0)
    {
        $jml = $this->db->get('tbl_user');
            
            $config['base_url'] = base_url().'master/admin';
            
            $config['total_rows'] = $jml->num_rows();
            $config['per_page'] = 6; /*Jumlah data yang dipanggil perhalaman*/  
            $config['uri_segment'] = 3; /*data selanjutnya di parse diurisegmen 3*/
            
            /*Class bootstrap pagination yang digunakan*/
            $config['full_tag_open'] = '<div class="pagination"><ul>';
            $config['full_tag_close'] = '</ul></div><!--pagination-->';

            $config['first_link'] = '&laquo; First';
            $config['first_tag_open'] = '<li class="prev page">';
            $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last &raquo;';
            $config['last_tag_open'] = '<li class="next page">';
            $config['last_tag_close'] = '</li>';

            $config['next_link'] = 'Next &rarr;';
            $config['next_tag_open'] = '<li class="next page">';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = '&larr; Previous';
            $config['prev_tag_open'] = '<li class="prev page">';
            $config['prev_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li class="page">';
            $config['num_tag_close'] = '</li>';
        
            $this->pagination->initialize($config);
            
            // $data['halaman'] = 
            /*membuat variable halaman untuk dipanggil di view nantinya*/
            // $data['offset'] = $offset;

            // $data['data_siswa'] = 
    	$data = array('title' =>'Master' ,
		'judul'=>'Data Petugas',
		'kd_user'=>$this->model_app->getKodePegawai(),
		//'data_pegawai'=>$this->model_app->getAllData('tbl_user'),
        'halaman'=>$this->pagination->create_links(),
        'offset'=>$offset,
        'data_pegawai'=>$this->model_app->view($config['per_page'], $offset),
         );
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		// $this->load->view('template/sidebar');
		$this->load->view('master/v_petugas',$data);
    }
    //end 


    //menambahkan data petugas
    public function tambah()
    {
        $data = array('title' =>'Master' ,
        'kd_user'=>$this->model_app->getKodePegawai(),
        'judul'=>'Tambah Karyawan' );
        // $this->load->view('template',$data);
        $this->load->view('tema/kepala',$data);
        $this->load->view('tema/navbar',$data);
        $this->load->view('master/tambah',$data);
    }
    //end

    //edit data petugas
    public function edit()
    {
        $data = array('title' =>'Master' ,
        'kd_user'=>$this->model_app->getKodePegawai(),
        'data_pegawai'=>$this->model_app->edit_user(),
        'judul'=>'Edit Petugas' );
        // $this->load->view('template',$data);
        $this->load->view('tema/kepala',$data);
        $this->load->view('tema/navbar',$data);
        $this->load->view('master/edit',$data);
    }
    //end

    //tambah petugas
     public function tambah_pegawai(){
        $data=array(
            'kd_user'=> $this->input->post('kd_user'),
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'nama'=> $this->input->post('nama'),
            'alamat'=> $this->input->post('alamat'),
            'divisi' => $this->input->post('divisi'),
            'jabatan' => $this->input->post('jabatan'),
            'tlp' => $this->input->post('tlp'),
            'level'=>$this->input->post('level'),
        );
        $this->model_app->insertData('tbl_user',$data);
        redirect("master/admin");
    }
    //end petugas

    //edit petugas
    function edit_pegawai(){
        $id['kd_user'] = $this->input->post('kd_user');
        $data=array(
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'nama'=> $this->input->post('nama'),
            'level'=>$this->input->post('level'),
        );
        $this->model_app->updateData('tbl_user',$data,$id);
        redirect("master/admin");
    }
    //end petugas

    //hapus petugas
    function hapus_pegawai(){
        $id['kd_user'] = $this->uri->segment(3);
        $this->model_app->deleteData('tbl_user',$id);
        redirect("master/petugas");
    }
    //hapus petugas

    //cari data petugas
    public function search($value='')
    {

        //$this->load->model('dev_auth_model');
        $value = $this->input->post('username');
        $data = array(
            'title' => 'Hasil Pencarian',
            'judul' => 'Pencarian Surat Masuk',
            'data_pegawai' => $this->model_app->search_petugas($value), );
        // $this->load->view('template',$data);
        $this->load->view('tema/kepala',$data);
        $this->load->view('tema/navbar',$data);
        $this->load->view('master/search',$data);
    }
    //end cari data
    // public function kop_surat(){
    //     $data = array('title' =>'Master' ,
    //     //'kd_user'=>$this->model_app->getKodePegawai(),
    //     'judul'=>'Penganturan Kop Surat' );
    //     $this->load->view('template',$data);
    //     $this->load->view('template/header',$data);
    //     $this->load->view('template/sidebar',$data);
    //     $this->load->view('master/kop_surat/index',$data);

    // }
}
 ?>