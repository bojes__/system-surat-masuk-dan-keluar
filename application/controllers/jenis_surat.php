<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Jenis_surat extends Ci_Controller
{
	
	function __construct()
	{
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
        $this->load->model('model_app');
        $this->load->library(array('pagination'));
        //$this->load->helper('currency_format_helper');
		$this->load->model('jenis_surat_model','js',true);
	}

	public function index()
	{
		$this->data();
	}
	public function data($id=null)
	{
		$count = count($this->js->itung_get_data());
		$config['base_url'] = base_url("jenis_surat/data");// base_url nya belum bisa
		$config['total_rows'] = $count; 
		$config['per_page'] = '2';
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		//inisialisasi config
		$this->pagination->initialize($config);
		$data = array(
			'title' => 'Jenis Surat',
			'judul' => 'Master Jenis Surat',
			// 'das'
			'data' => $this->js->get_data($config['per_page'],$id),
			// 'headerGrup' => 'Grafik berdasarkan Grup Hak Akses',
			'halaman' => $this->pagination->create_links(),
			);	
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		$this->load->view('Jenis_surat/data', $data);
	}
	

	public function add_jenis_surat()
	{
		$item = array(
			'id_jenis' => $this->js->get_kode_jenis() ,
			'tipe_surat' => $this->input->post('tipe_surat'),
			'skala_p' => $this->input->post('skala_p'), );
		$this->js->insertData('tbl_jenis_surat',$item);
        redirect("jenis_surat");
	}
	public function hapus()
	{
        $id['id_jenis'] = $this->uri->segment(3);
        $this->js->deleteData('tbl_jenis_surat',$id);
        redirect("jenis_surat");
	}
	public function edit()
	{
		$id['id_jenis'] = $this->input->post('id_jenis');
		$data = array(
			'tipe_surat' => $this->input->post('tipe_surat'),
			'skala_p' => $this->input->post('skala_p'),);

		$this->js->updateData('tbl_jenis_surat',$data,$id);
		redirect("jenis_surat");
	}
}
 ?>