<?php
class Login extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('model_app');

        $this->load->library('session');
    }
    // menampilkan halaman depan login
    function index(){
        if($this->session->userdata('login_status') != TRUE ){
            // $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !'); 
            // echo "Belom Login";
        $data=array(
            'title'=>'Login'
        );
        $this->load->view('tema/kepala',$data);
        // $this->load->view('tema/navbar',$data);
        $this->load->view('admin/v_login',$data);
            // redirect('');
        }else {
            redirect('dashboard');
        }
    }
    //end halaman depan login

    //cek login ketika masuk
    function cek_login() {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('kd_user');
        $password = $this->input->post('password');
        //query the database
        $result = $this->model_app->login($username, $password);
        if($result) {
            $sess_array = array();
            foreach($result as $row) {
                //create the session
                $sess_array = array(
                    'ID' => $row->kd_user,
                    'USERNAME' => $row->username,
                    'PASS'=>$row->password,
                    'NAME'=>$row->nama,
                    'LEVEL' => $row->level,
                    'login_status'=>true,
                );
                //set session with value from database
                $this->session->set_userdata($sess_array);
                redirect('dashboard','refresh');
            }
            return TRUE;
        } else {
            //if form validate false
            redirect('dashboard','refresh');
            return FALSE;
        }
    }
    //end login


    //untuk keluar dari aplikasi
    function logout() {
        $this->session->sess_destroy();
        $this->session->unset_userdata('ID');
        $this->session->unset_userdata('USERNAME');
        $this->session->unset_userdata('PASS');
        $this->session->unset_userdata('NAME');
        $this->session->unset_userdata('LEVEL');
        $this->session->unset_userdata('login_status');
        $this->session->set_flashdata('notif','LOGOUT BERHASIL');
        redirect('login');
    }

    function anu() { 
    if($this->session->userdata('login_status') == 'TRUE')
        {
   // $this->session->sess_destroy();
    echo $username;
        }
    }
}
