<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Divisi extends Ci_Controller
{
	
	function __construct()
	{
        parent::__construct();
        if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
        $this->load->model('model_app');
        $this->load->library(array('pagination'));
        //$this->load->helper('currency_format_helper');
		$this->load->model('divisi_model','dm',true);
	}

	public function index()
	{
		$this->data();
	}
	public function data($id=null)
	{
		$count = count($this->dm->itung_get_data());
		$config['base_url'] = base_url("divisi/data");// base_url nya belum bisa
		$config['total_rows'] = $count; 
		$config['per_page'] = '2';
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		//inisialisasi config
		$this->pagination->initialize($config);
		$data = array(
			'title' => 'Divisi',
			'judul' => 'Master Divisi',
			// 'das'
			'data' => $this->dm->get_data($config['per_page'],$id),
			// 'headerGrup' => 'Grafik berdasarkan Grup Hak Akses',
			'halaman' => $this->pagination->create_links(),
			);	
		// $this->load->view($this->config->item('kepala','devprog'),$data);
		// $this->load->view($this->config->item('navbar','devprog'),$data);
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		$this->load->view('divisi/data', $data);
		// $this->load->view('footer',$data);
	}

	public function add()
	{
		
		$item = array(
			'id_divisi' => $this->dm->get_kode_divisi(),
			'nama_divisi' => $this->input->post('nama_divisi'),
			'kepala_divisi' =>$this->input->post('kepala_divisi'), );
		$this->dm->insertData('tbl_divisi',$item);
        redirect("divisi");
	}
	public function hapus()
	{
        $id['id_divisi'] = $this->uri->segment(3);
        $this->dm->deleteData('tbl_divisi',$id);
        redirect("divisi");
	}
	public function edit()
	{
		$id['id_divisi'] = $this->input->post('id_divisi');
		$data = array(
			// 'id_divisi' => $this->input->post('id_divisi') ,
			'nama_divisi' => $this->input->post('nama_divisi'),
			'kepala_divisi' =>$this->input->post('kepala_divisi'), );

		$this->dm->updateData('tbl_divisi',$data,$id);
		redirect("divisi");
	}
}
 ?>