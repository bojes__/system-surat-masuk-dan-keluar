<?php 
/**
* 
*/
class Surat_masuk extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
		//$this->load->model('m_user');
		$this->load->library(array('form_validation','pagination'));
		$this->load->helper(array('language'));
		$this->load->model('model_surat_masuk','sm',true);
		$this->load->helper(array('url', 'form'));
	}
	public function index()
	{
		$this->data();
	}
	function data($offset=null)
	{
		$jml = $this->db->get('tbl_surat_masuk');
            
            $config['base_url'] = base_url().'surat_masuk/index';
            
            $config['total_rows'] = $jml->num_rows();
            $config['per_page'] = 5; /*Jumlah data yang dipanggil perhalaman*/  
            $config['uri_segment'] = 3; /*data selanjutnya di parse diurisegmen 3*/
            
            /*Class bootstrap pagination yang digunakan*/
            $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";
        
            $this->pagination->initialize($config);

		$data = array(
			'title' =>'Persuratan' ,
			'grup' => $this->sm->get_master_surat($config['per_page'],$offset),
			//'data_sk'=>$this->sm->getAllData('tbl_surat_masuk'),
			'data'=>$this->sm->ambil_data(),
			'offset'=>$offset,
			'pagination' => $this->pagination->create_links(),
			'judul'=>'Data Surat Masuk' );
		// $this->load->view('template',$data);
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		$this->load->view('surat_masuk/data',$data);
	}

	public function tambah()
	{
		$data = array('title' =>'Persuratan' ,
		'id'=>$this->sm->kode_suratmasuk(),
		'judul'=>'Input Surat Masuk' );
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		$this->load->view('surat_masuk/add',$data);
	}
	public function add()
	{
		$this->form_validation->set_rules('id','Id Surat','required');
		$this->form_validation->set_rules('asal_surat','Asal Surat','required');
		$this->form_validation->set_rules('tanggal_surat','Tanggal Surat','required');
		if ($this->form_validation->run() == FALSE) {

		$data = array('title' =>'Persuratan' ,
		'judul'=>'Input Surat Masuk' );
		// $data['pesan'] = $this(validation_errors() ? validation_errors('<div class="alert alert-danger">','</div>')
		$this->load->view('tema/kepala',$data);
		$this->load->view('tema/navbar',$data);
		$this->load->view('surat_masuk/add',$data);
		} else {
			$data = array(
				'id'		=>$this->input->post('id'),
				'asal_surat' =>$this->input->post('asal_surat') ,
			 	'tanggal_surat' =>$this->input->post('tanggal_surat'),);
			$this->db->insert('tbl_sr_masuk',$data);
			$this->session->set_flashdata('success','<div class="alert-danger">Data berhasil Tersimpan</div>');
			redirect('surat_masuk','refresh');
		}
		
	}
}