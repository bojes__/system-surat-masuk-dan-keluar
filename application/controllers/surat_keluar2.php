<?php 
/**
* 
*/
class Surat_keluar extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('login_status') != TRUE ){
            $this->session->set_flashdata('notif','LOGIN GAGAL USERNAME ATAU PASSWORD ANDA SALAH !');
            redirect('');
        };
		//$this->load->model('m_user');
		$this->load->model('model_surat_keluar','sk',true);
		$this->load->helper(array('url', 'form'));
	}
	//melihat data surat keluar dan pagination
	function index($offset=null)
	{
		$jml = $this->db->get('tbl_surat_keluar');
            
            $config['base_url'] = base_url().'surat_keluar/index';
            
            $config['total_rows'] = $jml->num_rows();
            $config['per_page'] = 3; /*Jumlah data yang dipanggil perhalaman*/  
            $config['uri_segment'] = 3; /*data selanjutnya di parse diurisegmen 3*/
            
            /*Class bootstrap pagination yang digunakan*/
            $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";
        
            $this->pagination->initialize($config);


		$data = array(
			'title' =>'Persuratan' ,
			'surat_keluar' => $this->sk->get_master_surat($config['per_page'],$offset),
			//'data_sk'=>$this->sm->getAllData('tbl_surat_masuk'),
			'offset'=>$offset,
			'pagination' => $this->pagination->create_links(),
			//'data_sk'=>$this->sk->getAllData('tbl_surat_keluar'),
			'judul'=>'Data Surat Keluar' );
		$this->load->view('template',$data);
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('surat_keluar/index',$data);
	}
	//end

	//tambah surat keluar
	public function tambah()
	{
		$data = array('title' =>'Persuratan' ,
		'judul'=>'Input Surat Keluar' );
		$this->load->view('template',$data);
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('surat_keluar/tambah');
	}
	//end


	//edit surat keluar
	public function edit_sk()
	{
		$id['id_surat_keluar'] = $this->input->post('id_surat_keluar');
		$data = array(
			//'id_surat_keluar' =>$this->sk->getKode_acak_suratkeluar() ,
			'no_agenda'=>$this->input->post('no_agenda'),
			'tgl'=>$this->input->post('tgl'),
			'no_surat'=>$this->input->post('no_surat'),
			'tujuan'=>$this->input->post('tujuan'),
			'perihal'=>$this->input->post('perihal'),
			'sifat_surat'=>$this->input->post('sifat_surat') );

		$this->sk->updateData('tbl_surat_keluar',$data,$id);
        redirect("surat_keluar");	
	}
	//end edit surat keluar

	//tambah surat keluar
	public function save()
	{
		$data = array(
			'id_surat_keluar' =>$this->sk->getKode_acak_suratkeluar() ,
			'no_agenda'=>$this->input->post('no_agenda'),
			'tgl'=>$this->input->post('tgl'),
			'no_surat'=>$this->input->post('no_surat'),
			'tujuan'=>$this->input->post('tujuan'),
			'perihal'=>$this->input->post('perihal'),
			'sifat_surat'=>$this->input->post('sifat_surat') );

		$this->sk->insertData('tbl_surat_keluar',$data);
        redirect("surat_keluar");
	}
	//end tambah surat keluar
	public function hapus(){
        $id['id_surat_keluar'] = $this->uri->segment(3);
        $this->sk->deleteData('tbl_surat_keluar',$id);
        redirect("surat_keluar");
    }
    //cari surat keluar
    public function search($value='')
	{
		//$this->load->model('dev_auth_model');
		$value = $this->input->post('no_surat');
		$data = array(
			'title' => 'Hasil Pencarian',
			'judul' => 'Pencarian Surat Masuk',
			'surat_keluar' => $this->sk->search_keluar($value), );
		$this->load->view('template',$data);
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('surat_keluar/search',$data);
	}
	//end cari

	//lihat data laporan
	 public function laporan_suratkeluar()
	{
		$data = array('title' =>'Laporan' ,
		'judul'=>'Laporan Surat Keluar' );
		$this->load->view('template',$data);
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('surat_keluar/laporan/index',$data);
	}
	//end laporan

	//cetak surat keluar
	public function laporan()
	{
		//$this->load->model('dev_auth_model');
		// $value = $this->input->post('tgl_surat');
		$value = $this->input->post('sifat_surat');
		$anu = $this->input->post('optionsRadios');
		if (empty($value) || $value == 'null' || $anu=='all') {

		$data = array(
			'title' => 'Cetak Surat Keluar',
			'judul' => 'AGENDA SURAT MASUK',
			'data' => $this->sk->cetak_masuk($anu), );
		$this->load->view('template',$data);
		//$this->load->view('template/header',$data);
		//$this->load->view('template/sidebar',$data);
		$this->load->view('surat_keluar/laporan/laporan',$data);
		} else {

		$data = array(
			'title' => 'Cetak Surat Keluar',
			'judul' => 'AGENDA SURAT MASUK',
			'data' => $this->sk->cetak_masuk($value), );
		$this->load->view('template',$data);
		//$this->load->view('template/header',$data);
		//$this->load->view('template/sidebar',$data);
		$this->load->view('surat_keluar/laporan/laporan',$data);
		}
	}
	//end cetak surat keluar
}

 ?>