<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Divisi_model extends CI_Model
{
	public function get_kode_divisi(){
        $q = $this->db->query("select MAX(RIGHT(id_divisi,03)) as code_max from tbl_divisi");
        $code = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $cd) {
                $tmp = ((int) $cd->code_max) + 1;
                $code = sprintf("%03s", $tmp);
            }
        } else {
            $code = "001";
        }
        return "DV-".$code;
    }

	public function get_data($num,$offset,$itung = null)
	{
		$query = $this->db->select('*')
		->from('tbl_divisi')
		->limit($num,$offset)
		->get();
		return $query->result_array();
	}
	public function itung_get_data()
	{
		$query = $this->db->select('*')
		->from('tbl_divisi')
		->get();
		return $query->result_array();
	}

	function insertData($table,$data)
    {
        $this->db->insert($table,$data);
    }
    function deleteData($table,$data)
    {
        $this->db->delete($table,$data);
    }
    function updateData($table,$data,$field_key)
    {
        $this->db->update($table,$data,$field_key);
    }
}
 ?>