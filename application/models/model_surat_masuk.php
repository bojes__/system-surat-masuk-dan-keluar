<?php 
/**
* 
*/
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_surat_masuk extends CI_Model
{
	//surat masuk
	function getKode_acak_suratmasuk() {
        $q = $this->db->query("select MAX(RIGHT(id_surat_masuk,10)) as code_max from tbl_surat_masuk");
        $code = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $cd) {
                $tmp = ((int) $cd->code_max) + 1;
                $code = sprintf("%010s", $tmp);
            }
        } else {
            $code = "01";
        }
        return $code;
    }
    function kode_suratmasuk() {
        $q = $this->db->query("select MAX(RIGHT(id,3)) as code_max from tbl_sr_masuk");
        $code = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $cd) {
                $tmp = ((int) $cd->code_max) + 1;
                $code = sprintf("%03s", $tmp);
            }
        } else {
            $code = "001";
        }
       return "SM-".$code;
    }
    public function ambil_data()
    {
        $query = $this->db->select('*')
                ->from('tbl_sr_masuk')
                ->get();
        return $query->result_array();
    }
    public function get_sosmed($limit,$offset)
    {
        return $this->db->order_by("id_surat_masuk","asc")
                        ->limit($limit,$offset)
                        ->get("tbl_surat_masuk");
    }
    public function suratmasuk_num_rows()
    {
        return $this->db
                    ->get('tbl_surat_masuk')
                    ->num_rows();
    }
    public function get_master_surat($num,$offset)
    {
        $query = $this->db->select('*')
                        //->from('groups')
                          ->get('tbl_surat_masuk',$num,$offset);
        return $query->result_array();
    }
    public function cetak_surat()
    {
        $query = $this->db->select('*')
                          ->from('tbl_surat_masuk')
                          ->get();
        return $query->result_array();
    }


    public function getAllData($table)
    {
        return $this->db->get($table)->result();
    }
    public function getSelectedData($table,$data)
    {
        return $this->db->get_where($table, $data);
    }
    function updateData($table,$data,$field_key)
    {
        $this->db->update($table,$data,$field_key);
    }
    function deleteData($table,$data)
    {
        $this->db->delete($table,$data);
    }
    function insertData($table,$data)
    {
        $this->db->insert($table,$data);
    }

    public function save_suratmasuk($id_surat_masuk,$no_agenda,$tgl_diterima,$nama_pengirim,$no_surat,$tgl_surat,$perihal,$sifat_surat)
    {
    	$item = array(
    		'id_surat_masuk' =>$id_surat_masuk ,
    	    'no_agenda'=>$no_agenda,
    	    'tgl_diterima'=>$tgl_diterima,
    	    'nama_pengirim'=>$nama_pengirim,
    	    'no_surat'=>$no_surat,
    	    'tgl_surat'=>$tgl_surat,
    	    'perihal'=>$perihal,
    	    'sifat_surat'=>$sifat_surat);
    	return $this->db->insert("tbl_surat_masuk",$item);
    	redirect('index');

    }
    public function delete_sosmed()
    {
        $id_surat_masuk=$_GET['id_surat_masuk'];
        return $this->db->where("id_surat_masuk",$id_surat_masuk)
                        ->delete("tbl_surat_masuk");
    }
    public function search_masuk($value='')
    {
        $query = $this->db->select('*')
        ->from('tbl_surat_masuk')
        ->like('no_surat',$value)
        ->get();
        return $query->result_array();
    }
    public function cetak_masuk($value='')
    {
        if ($value == 'all') {
         $query = $this->db->select('*')
        ->from('tbl_surat_masuk')
        // ->like('sifat_surat',$value)
        ->get();
        return $query->result_array();   

        } else {
            
        $query = $this->db->select('*')
        ->from('tbl_surat_masuk')
        ->like('sifat_surat',$value)
        ->get();
        return $query->result_array();
        }
    }
}
 ?>