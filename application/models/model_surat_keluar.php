<?php 
/**
* 
*/
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_surat_keluar extends CI_Model
{

	// id surat keluar
	function getKode_acak_suratkeluar() {
        $q = $this->db->query("select MAX(RIGHT(id_surat_keluar,10)) as code_max from tbl_surat_keluar");
        $code = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $cd) {
                $tmp = ((int) $cd->code_max) + 1;
                $code = sprintf("%010s", $tmp);
            }
        } else {
            $code = "01";
        }
        return $code;
    }
    //end id surat keluar

    public function get_master_surat($num,$offset)
    {
        $query = $this->db->select('*')
        // ->from('groups')
        ->get('tbl_surat_keluar',$num,$offset);
        return $query->result_array();
    }
    
    public function getAllData($table)
    {
        return $this->db->get($table)->result();
    }
    public function getSelectedData($table,$data)
    {
        return $this->db->get_where($table, $data);
    }
    function updateData($table,$data,$field_key)
    {
        $this->db->update($table,$data,$field_key);
    }
    function deleteData($table,$data)
    {
        $this->db->delete($table,$data);
    }
    function insertData($table,$data)
    {
        $this->db->insert($table,$data);
    }
    //pencarian berdasarkan nomor surat
    public function search_keluar($value='')
    {
        $query = $this->db->select('*')
        ->from('tbl_surat_keluar')
        ->like('no_surat',$value)
        ->get();
        return $query->result_array();
    }
    //end pencarian

    // public function cetak_surat()
    // {
    //     $query = $this->db->select('*')
    //                       ->from('tbl_surat_keluar')
    //                       ->get();
    //     return $query->result_array();
    // }

    //cetak surat masuk
    public function cetak_masuk($value='')
    {
        if ($value == 'all') {
         $query = $this->db->select('*')
        ->from('tbl_surat_keluar')
        // ->like('sifat_surat',$value)
        ->get();
        return $query->result_array();   

        } else {
            
        $query = $this->db->select('*')
        ->from('tbl_surat_keluar')
        ->like('sifat_surat',$value)
        ->get();
        return $query->result_array();
        }
    }
    //end cetak surat masuk
}
 ?>