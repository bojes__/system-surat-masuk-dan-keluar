<?php

class Model_app extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    
    //    KODE PEGAWAI
    public function getKodePegawai(){
        $q = $this->db->query("select MAX(RIGHT(kd_user,3)) as kd_max from tbl_user");
        $kd = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%03s", $tmp);
            }
        }else{
            $kd = "001";
        }
        return "K-".$kd;
    }

    function login($username, $password) {
        //create query to connect user login database
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('username', $username);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);

        //get query and processing
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return $query->result(); //if data is true
        } else {
            return false; //if data is wrong
        }
    }
    
    public function view($num, $offset)
    {
        $query = $this->db->select('*')
        // ->from('groups')
        ->get('tbl_user',$num,$offset);
        return $query->result_array();

        
    }
    public function edit_user()
    {
        $query = $this->db->select('*')
        ->get('tbl_user');
        return $query->result_array();
    }
    public function getAllData($table)
    {
        return $this->db->get($table)->result();
    }
    public function getSelectedData($table,$data)
    {
        return $this->db->get_where($table, $data);
    }
    function updateData($table,$data,$field_key)
    {
        $this->db->update($table,$data,$field_key);
    }
    function deleteData($table,$data)
    {
        $this->db->delete($table,$data);
    }
    function insertData($table,$data)
    {
        $this->db->insert($table,$data);
    }

    //pencarian petugas
    public function search_petugas($value='')
    {
        $query = $this->db->select('*')
        ->from('tbl_user')
        ->like('username',$value)
        ->get();
        return $query->result_array();
    }
    //end pencarian petugas



}