-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 29, 2015 at 11:44 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_arsip`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_divisi`
--

CREATE TABLE IF NOT EXISTS `tbl_divisi` (
  `id_divisi` varchar(10) NOT NULL,
  `nama_divisi` varchar(100) NOT NULL,
  `kepala_divisi` varchar(100) NOT NULL,
  PRIMARY KEY (`id_divisi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_divisi`
--

INSERT INTO `tbl_divisi` (`id_divisi`, `nama_divisi`, `kepala_divisi`) VALUES
('DV-001', 'manager', 'muhammad azis s'),
('DV-002', 'keuangan', 'komeng');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_surat`
--

CREATE TABLE IF NOT EXISTS `tbl_jenis_surat` (
  `id_jenis` varchar(10) NOT NULL,
  `tipe_surat` varchar(100) NOT NULL,
  `skala_p` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jenis_surat`
--

INSERT INTO `tbl_jenis_surat` (`id_jenis`, `tipe_surat`, `skala_p`) VALUES
('JS-001', 'anuajah', 'ahaha'),
('JS-002', 'undangan', 'muhammad azis s');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_perusahaan`
--

CREATE TABLE IF NOT EXISTS `tbl_perusahaan` (
  `id_perusahaan` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(50) NOT NULL,
  `tlp` varchar(15) NOT NULL,
  PRIMARY KEY (`id_perusahaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_perusahaan`
--

INSERT INTO `tbl_perusahaan` (`id_perusahaan`, `nama`, `alamat`, `kota`, `tlp`) VALUES
('P-001', 'deprog', 'kali abru', 'bekasi', '9797'),
('P-003', 'jajal', 'jsjsj', 'm,m,', 'mm,');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surat_keluar`
--

CREATE TABLE IF NOT EXISTS `tbl_surat_keluar` (
  `id_surat_keluar` varchar(10) NOT NULL,
  `no_agenda` varchar(30) NOT NULL,
  `tgl` varchar(20) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `perihal` text NOT NULL,
  `sifat_surat` varchar(15) NOT NULL,
  PRIMARY KEY (`id_surat_keluar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_surat_keluar`
--

INSERT INTO `tbl_surat_keluar` (`id_surat_keluar`, `no_agenda`, `tgl`, `no_surat`, `tujuan`, `perihal`, `sifat_surat`) VALUES
('0000000002', 'n n', '04/16/2015', 'n n mm', 'mmm', '<p>mmmaaaa hahahah</p>', 'Biasa'),
('0000000003', 'knkasdns', '05/12/2015', 'sds', 'dsd', '<p>warno sedang belajar</p>', 'Biasa'),
('0000000004', 'haha', '05/12/2015', 'heheh', 'hoihoho', '<p>dsadsdsd</p>', 'Rahasia');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surat_masuk`
--

CREATE TABLE IF NOT EXISTS `tbl_surat_masuk` (
  `id_surat_masuk` varchar(10) NOT NULL,
  `no_agenda` varchar(50) NOT NULL,
  `tgl_diterima` varchar(30) NOT NULL,
  `nama_pengirim` varchar(100) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `tgl_surat` varchar(30) NOT NULL,
  `perihal` text NOT NULL,
  `sifat_surat` varchar(30) NOT NULL,
  PRIMARY KEY (`id_surat_masuk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_surat_masuk`
--

INSERT INTO `tbl_surat_masuk` (`id_surat_masuk`, `no_agenda`, `tgl_diterima`, `nama_pengirim`, `no_surat`, `tgl_surat`, `perihal`, `sifat_surat`) VALUES
('0000000002', 'ad', '04/27/2015', 'ajis', 'nnss', '05/09/2015', '<p>saya adaah</p>', 'Biasa'),
('0000000003', 'jbjbahaha', '04/22/2015', 'nnknbkbk', 'bkbkk', '04/12/2015', '<p style="text-align: center;">nknknknknknk</p>', 'Biasa'),
('0000000004', '7jhaj', '04/13/2015', 'sds', 'sds', '05/09/2015', '', 'Pengantar'),
('0000000005', 'nk', '05/05/2015', 'askas', 'sasa', '05/20/2015', '<p>saya ingin belajar php</p>', 'Pengantar'),
('0000000006', 'nkmn', '05/05/2015', 'mnn', 'mmj', '05/13/2015', '<p>mm</p>', 'Biasa'),
('0000000007', 'nn', '05/26/2015', 'nn', 'jj', '05/28/2015', '<p>jjj</p>', 'Pengantar'),
('0000000008', 'dsd', '06/18/2015', 'xscx', 'xcx', '06/02/2015', '<p>cxcxc</p>', 'Biasa'),
('0000000009', 'jaja', '06/20/2015', 'saya', '111', '06/21/2015', '<p>jajal</p>', 'Undangan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `kd_user` varchar(5) NOT NULL DEFAULT '0',
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `alamat` text NOT NULL,
  `divisi` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `tlp` varchar(15) NOT NULL,
  `level` enum('user','admin') DEFAULT 'user',
  PRIMARY KEY (`kd_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`kd_user`, `username`, `password`, `nama`, `alamat`, `divisi`, `jabatan`, `tlp`, `level`) VALUES
('K-003', 'bojes', '601dc66f3a0d8ffb213841a5dd5fb59b', 'bojes', '', '', '', '', 'user'),
('K-004', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'muhammad azis s`', '', '', '', '', 'admin'),
('K-005', 'users', '9bc65c2abec141778ffaa729489f3e87', 'users', '', '', '', '', 'admin'),
('K-008', 'asaya', '20c1a26a55039b30866c9d0aa51953ca', 'aku', '', '', '', '', 'admin'),
('K-009', '201210', '974151735a3af9f0623c60097559b6f0', 'muhamad', '', '', '', '', 'user'),
('K-010', '10', '20e43b79d8e1f2c9424239455e3aed01', 'saya', 'hkhk', 'anu', 'jajal', '7888', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
